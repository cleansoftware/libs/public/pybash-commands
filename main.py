import json
from pybash_commands.docker_node import l18_docker_node
from pybash_commands.docker_service import l18_docker_service
from pybash_commands.disk_free import df_max_info_to_list_of_dict

if __name__ == '__main__':
    print(json.dumps(l18_docker_node(), indent=4))
    print(json.dumps(l18_docker_service(), indent=4))
    print(json.dumps(df_max_info_to_list_of_dict(), indent=4))








