from bash import bash
from pybash_commands.inmutables import _Commands
from pybash_commands.inmutables import _mapper_column_docker_service as ds_mapper
from pybash_commands.core import _clean_head


def _docker_service_to_str() -> bash:
    stream = _clean_head(_Commands.DOCKER_SERVICE)
    return stream


def l18_docker_service() -> dict:
    list_services: list = str(_docker_service_to_str()).split('\n')
    lines: list = []
    for line in list_services:
        temp: dict = {
            str(ds_mapper['1']): str(bash(f"echo \"{line}\"").bash("awk '{print $1}'")),
            str(ds_mapper['2']): str(bash(f"echo \"{line}\"").bash("awk '{print $2}'")),
            str(ds_mapper['3']): str(bash(f"echo \"{line}\"").bash("awk '{print $3}'")),
            str(ds_mapper['4']): str(bash(f"echo \"{line}\"").bash("awk '{print $4}'")),
            str(ds_mapper['5']): str(bash(f"echo \"{line}\"").bash("awk '{print $5}'")),
        }
        lines.append(temp)
    return lines

