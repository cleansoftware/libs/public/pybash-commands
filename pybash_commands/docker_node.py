from bash import bash

from pybash_commands.core import _clean_head
from pybash_commands.inmutables import _Commands
from pybash_commands.inmutables import _mapper_column_docker_node as dn_mapper

def _docker_service_to_list() -> list:
    stream_list: list = str(_clean_head(_Commands.DOCKER_NODE)).replace('*', '').split('\n')
    return stream_list


def l18_docker_node():
    list_nodes: list = _docker_service_to_list()
    node_list: list = []
    for node in list_nodes:
        temp: dict = {
            str(dn_mapper['1']): str(bash(f'echo {node}').bash("awk '{print $1}'")),
            str(dn_mapper['2']): str(bash(f'echo {node}').bash("awk '{print $2}'")),
            str(dn_mapper['3']): str(bash(f'echo {node}').bash("awk '{print $3}'")),
            str(dn_mapper['4']): str(bash(f'echo {node}').bash("awk '{print $4}'")),
            str(dn_mapper['5']): str(bash(f'echo {node}').bash("awk '{print $5}'")),
            str(dn_mapper['6']): str(bash(f'echo {node}').bash("awk '{print $6}'"))
        }
        node_list.append(temp)
    return node_list
