from backports.strenum import StrEnum


class _Commands(StrEnum):
    DF_MAX_INF = 'df -PhTv'
    CLEAN_HEAD = 'tail -n +2'
    DOCKER_SERVICE = 'docker service ls'
    DOCKER_NODE = 'docker node ls'


class _ColummnsName(StrEnum):
    ID = 'id'
    NAME = 'name'
    MODE = 'mode'
    TYPE = 'type'
    SIZE = 'size'
    USED = 'used'
    DISP = 'disp'
    IMAGE = 'image'
    PORTS = 'ports'
    STATUS = 'status'
    HOSTNAME = 'hostname'
    REPLICAS = 'replicas'
    ID_SERVICE = 'id_service'
    MOUNTED_IN = 'mounted_in'
    SOURCE_FILE = 'source_file'
    AVAILABILITY = 'abailability'
    USED_PERCERNT = 'used_percent'
    MANAGER_STATUS = 'manager_status'
    ENGINE_VERSION = 'engine_version'


_mapper_column_docker_node: dict = {
    '1': str(_ColummnsName.ID_SERVICE),
    '2': str(_ColummnsName.HOSTNAME),
    '3': str(_ColummnsName.STATUS),
    '4': str(_ColummnsName.AVAILABILITY),
    '5': str(_ColummnsName.MANAGER_STATUS),
    '6': str(_ColummnsName.ENGINE_VERSION)
}

_mapper_column_docker_service: dict = {
    '1': str(_ColummnsName.ID_SERVICE),
    '2': str(_ColummnsName.NAME),
    '3': str(_ColummnsName.MODE),
    '4': str(_ColummnsName.REPLICAS),
    '5': str(_ColummnsName.IMAGE),
    '6': str(_ColummnsName.PORTS),
}

_mapped_column_df_max_inf: dict = {
    '1': str(_ColummnsName.SOURCE_FILE),
    '2': str(_ColummnsName.TYPE),
    '3': str(_ColummnsName.SIZE),
    '4': str(_ColummnsName.USED),
    '5': str(_ColummnsName.DISP),
    '6': str(_ColummnsName.USED_PERCERNT),
    '7': str(_ColummnsName.MOUNTED_IN),
    '8': str(_ColummnsName.SOURCE_FILE)
}
