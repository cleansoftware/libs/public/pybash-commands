from bash import bash
from pybash_commands.inmutables import _Commands
from pybash_commands.inmutables import _mapped_column_df_max_inf
from pybash_commands.core import _clean_head


def _df_max_info_to_string() -> bash:
    return _clean_head(_Commands.DF_MAX_INF)


def _df_max_info_to_list() -> list:
    df_list: list = str(_df_max_info_to_string()).split('\n')
    return df_list


def df_max_info_to_list_of_dict() -> list:
    df_list: list = _df_max_info_to_list()
    result_list: list = []
    for index in range(len(df_list)):
        temp_dict: dict = {
            str(_mapped_column_df_max_inf['1']): str(bash(f"echo {df_list[index]}").bash("awk '{print $1}'")),
            str(_mapped_column_df_max_inf['2']): str(bash(f"echo {df_list[index]}").bash("awk '{print $2}'")),
            str(_mapped_column_df_max_inf['3']): str(bash(f"echo {df_list[index]}").bash("awk '{print $3}'")),
            str(_mapped_column_df_max_inf['4']): str(bash(f"echo {df_list[index]}").bash("awk '{print $4}'")),
            str(_mapped_column_df_max_inf['5']): str(bash(f"echo {df_list[index]}").bash("awk '{print $5}'")),
            str(_mapped_column_df_max_inf['6']): str(bash(f"echo {df_list[index]}").bash("awk '{print $6}'")),
            str(_mapped_column_df_max_inf['7']): str(bash(f"echo {df_list[index]}").bash("awk '{print $7}'")),
        }
        result_list.append(temp_dict)
    return result_list
