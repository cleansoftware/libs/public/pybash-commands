from bash import bash
from pybash_commands.inmutables import _Commands

def _clean_head(command: str) -> bash:
    return bash(command).bash(_Commands.CLEAN_HEAD)