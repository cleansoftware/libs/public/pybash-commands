import setuptools

setuptools.setup(
    name="pybash-commands",
    version="0.1.0",
    author="Daniel Rodriguez Rodriguez",
    author_email="danielrodriguezrodriguez.pks@gmail.com",
    description="",
    url="",
    project_urls={
        "Bug Tracker": "",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=['pybash_commands'],
    install_requires=[
        'bash==0.6',
        'requests-facade@https://gitlab.com/cleansoftware/libs/public/requests-facade/-/raw/master/dist/requests-facade-0.1.0.tar.gz'
    ],
    python_requires=">=3.9",
)

